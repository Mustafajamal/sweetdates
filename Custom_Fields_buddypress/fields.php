<?php
/*
Plugin Name: Buddypress Register fields
Plugin URI: http://agilesolutionspk.com/training
Description: It is just for thrill
Version: 1.0
Author URI: 
*/
 
//add_action('bp_signup_profile_fields', function() {


do_action( 'bp_before_account_details_fields', function(){
    
	ob_start();
		echo '<div id="steps_btn">
		<span id="step1">Step 1</span>
		<span id="step2">Step 2</span>
		</div>';
	$reg_steps = ob_get_clean();
		
    return $reg_steps;
});



add_action('bp_account_details_fields', function() {
    global $twitter_field_value; ?>
 
    <div class="editfield field_address required-field visibility-public field_type_text" id="title-field">
        <label for="title"><?php _e('Title', 'my-plugin'); ?> 
            <span class="bp-required-field-label"><?php _e('(required)', 'my-plugin'); ?></span>
        </label>
        <?php do_action( 'bp_title_errors' ); ?>
        <?php $title_field_value = esc_attr($title_field_value); ?>		
		<select id="title" name="title" >
		<?php if ($title_field_value) echo '<option value="'. $title_field_value.'" selected>'. $title_field_value.'</option>';?>
			<option class="option" value="">--- Select ---</option>
			<option class="option" value="mr">Mr.</option>
			<option class="option" value="miss">Miss.</option>
			<option class="option" value="mrs">Mrs.</option>
		</select>
    </div>
    
	<div class="editfield field_address required-field visibility-public field_type_text" id="first_name-field">
        <label for="first_name"><?php _e('First Name', 'my-plugin'); ?> 
            <span class="bp-required-field-label"><?php _e('(required)', 'my-plugin'); ?></span>
        </label>
        <?php do_action( 'bp_first_name_errors' ); ?>
        <input type="text" id="first_name" name="first_name" value="<?php echo esc_attr($first_name_field_value); ?>" />
    </div>
    
	<div class="editfield field_address required-field visibility-public field_type_text" id="last_name-field">
        <label for="last_name"><?php _e('Last Name', 'my-plugin'); ?> 
            <span class="bp-required-field-label"><?php _e('(required)', 'my-plugin'); ?></span>
        </label>
        <?php do_action( 'bp_last_name_errors' ); ?>
        <input type="text" id="last_name" name="last_name" value="<?php echo esc_attr($last_name_field_value); ?>" />
    </div>
	
	<div class="editfield field_address required-field visibility-public field_type_text" id="address-field">
        <label for="address"><?php _e('Address', 'my-plugin'); ?> 
            <span class="bp-required-field-label"><?php _e('(required)', 'my-plugin'); ?></span>
        </label>
        <?php do_action( 'bp_address_errors' ); ?>
        <input type="text" id="address" name="address" value="<?php echo esc_attr($address_field_value); ?>" />
    </div>
	
	<div class="editfield field_telephone required-field visibility-public field_type_text" id="telephone-field">
        <label for="telephone"><?php _e('Telephone', 'my-plugin'); ?> 
            <span class="bp-required-field-label"><?php _e('(required)', 'my-plugin'); ?></span>
        </label>
        <?php do_action( 'bp_telephone_errors' ); ?>
        <input type="text" id="telephone" name="telephone" value="<?php echo esc_attr($telephone_field_value); ?>" />
    </div>

	<div class="editfield field_city required-field visibility-public field_type_text" id="city-field">
        <label for="city"><?php _e('City', 'my-plugin'); ?> 
            <span class="bp-required-field-label"><?php _e('(required)', 'my-plugin'); ?></span>
        </label>
        <?php do_action( 'bp_city_errors' ); ?>
        <input type="text" id="city" name="city" value="<?php echo esc_attr($city_field_value); ?>" />
    </div>

	<div class="editfield field_country required-field visibility-public field_type_text" id="country-field">
        <label for="country"><?php _e('Country', 'my-plugin'); ?> 
            <span class="bp-required-field-label"><?php _e('(required)', 'my-plugin'); ?></span>
        </label>
        <?php do_action( 'bp_country_errors' ); ?>
		<?php $country_field_value = esc_attr($country_field_value); ?>
        <select type="text" id="country" name="country">
<?php if ($country_field_value) echo '<option value="'. $country_field_value.'" selected>'. $country_field_value.'</option>';?>
		<option class="option" value="United Arab Emirates">United Arab Emirates</option>
<option class="option" value="Albania">Albania</option>
<option class="option" value="Algeria">Algeria</option>
<option class="option" value="American Samoa">American Samoa</option>
<option class="option" value="Andorra">Andorra</option>
<option class="option" value="Angola">Angola</option>
<option class="option" value="Anguilla">Anguilla</option>
<option class="option" value="Antarctica">Antarctica</option>
<option class="option" value="Antigua and Barbuda">Antigua and Barbuda</option>
<option class="option" value="Argentina">Argentina</option>
<option class="option" value="Armenia">Armenia</option>
<option class="option" value="Aruba">Aruba</option>
<option class="option" value="Australia">Australia</option>
<option class="option" value="Austria">Austria</option>
<option class="option" value="Azerbaijan">Azerbaijan</option>
<option class="option" value="Bahamas">Bahamas</option>
<option class="option" value="Bahrain">Bahrain</option>
<option class="option" value="Bangladesh">Bangladesh</option>
<option class="option" value="Barbados">Barbados</option>
<option class="option" value="Belarus">Belarus</option>
<option class="option" value="Belgium">Belgium</option>
<option class="option" value="Belize">Belize</option>
<option class="option" value="Benin">Benin</option>
<option class="option" value="Bermuda">Bermuda</option>
<option class="option" value="Bhutan">Bhutan</option>
<option class="option" value="Bolivia">Bolivia</option>
<option class="option" value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
<option class="option" value="Botswana">Botswana</option>
<option class="option" value="Bouvet Island">Bouvet Island</option>
<option class="option" value="Brazil">Brazil</option>
<option class="option" value="Brunei Darussalam">Brunei Darussalam</option>
<option class="option" value="Bulgaria">Bulgaria</option>
<option class="option" value="Burkina Faso">Burkina Faso</option>
<option class="option" value="Burundi">Burundi</option>
<option class="option" value="Cambodia">Cambodia</option>
<option class="option" value="Cameroon">Cameroon</option>
<option class="option" value="Canada">Canada</option>
<option class="option" value="Cape Verde">Cape Verde</option>
<option class="option" value="Cayman Islands">Cayman Islands</option>
<option class="option" value="Central African Republic">Central African Republic</option>
<option class="option" value="Chad">Chad</option>
<option class="option" value="Chile">Chile</option>
<option class="option" value="China">China</option>
<option class="option" value="Christmas Island">Christmas Island</option>
<option class="option" value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
<option class="option" value="Colombia">Colombia</option>
<option class="option" value="Comoros">Comoros</option>
<option class="option" value="Congo">Congo</option>
<option class="option" value="Cook Islands">Cook Islands</option>
<option class="option" value="Costa Rica">Costa Rica</option>
<option class="option" value="Cote optionoire">Cote optionoire</option>
<option class="option" value="Croatia">Croatia</option>
<option class="option" value="Cuba">Cuba</option>
<option class="option" value="Cyprus">Cyprus</option>
<option class="option" value="Czech Republic">Czech Republic</option>
<option class="option" value="Denmark">Denmark</option>
<option class="option" value="Djibouti">Djibouti</option>
<option class="option" value="Dominica">Dominica</option>
<option class="option" value="Dominican Republic">Dominican Republic</option>
<option class="option" value="East Timor">East Timor</option>
<option class="option" value="Ecuador">Ecuador</option>
<option class="option" value="Egypt">Egypt</option>
<option class="option" value="El Salvador">El Salvador</option>
<option class="option" value="Equatorial Guinea">Equatorial Guinea</option>
<option class="option" value="Eritrea">Eritrea</option>
<option class="option" value="Estonia">Estonia</option>
<option class="option" value="Ethiopia">Ethiopia</option>
<option class="option" value="Faroe Islands">Faroe Islands</option>
<option class="option" value="Fiji">Fiji</option>
<option class="option" value="Finland">Finland</option>
<option class="option" value="France">France</option>
<option class="option" value="France Metropolitan">France Metropolitan</option>
<option class="option" value="French Guiana">French Guiana</option>
<option class="option" value="French Polynesia">French Polynesia</option>
<option class="option" value="Gabon">Gabon</option>
<option class="option" value="Gambia">Gambia</option>
<option class="option" value="Georgia">Georgia</option>
<option class="option" value="Germany">Germany</option>
<option class="option" value="Ghana">Ghana</option>
<option class="option" value="Gibraltar">Gibraltar</option>
<option class="option" value="Greece">Greece</option>
<option class="option" value="Greenland">Greenland</option>
<option class="option" value="Grenada">Grenada</option>
<option class="option" value="Guadeloupe">Guadeloupe</option>
<option class="option" value="Guam">Guam</option>
<option class="option" value="Guatemala">Guatemala</option>
<option class="option" value="Guinea">Guinea</option>
<option class="option" value="Guinea-bissau">Guinea-bissau</option>
<option class="option" value="Guyana">Guyana</option>
<option class="option" value="Haiti">Haiti</option>
<option class="option" value="Honduras">Honduras</option>
<option class="option" value="Hong Kong">Hong Kong</option>
<option class="option" value="Hungary">Hungary</option>
<option class="option" value="Iceland">Iceland</option>
<option class="option" value="India">India</option>
<option class="option" value="Indonesia">Indonesia</option>
<option class="option" value="Iran">Iran</option>
<option class="option" value="Iraq">Iraq</option>
<option class="option" value="Ireland">Ireland</option>
<option class="option" value="Italy">Italy</option>
<option class="option" value="Jamaica">Jamaica</option>
<option class="option" value="Japan">Japan</option>
<option class="option" value="Jordan">Jordan</option>
<option class="option" value="Kazakhstan">Kazakhstan</option>
<option class="option" value="Kenya">Kenya</option>
<option class="option" value="Kiribati">Kiribati</option>
<option class="option" value="Korea">Korea</option>
<option class="option" value="Kuwait">Kuwait</option>
<option class="option" value="Kyrgyzstan">Kyrgyzstan</option>
<option class="option" value="Latvia">Latvia</option>
<option class="option" value="Lebanon">Lebanon</option>
<option class="option" value="Lesotho">Lesotho</option>
<option class="option" value="Liberia">Liberia</option>
<option class="option" value="Libya">Libya</option>
<option class="option" value="Liechtenstein">Liechtenstein</option>
<option class="option" value="Lithuania">Lithuania</option>
<option class="option" value="Luxembourg">Luxembourg</option>
<option class="option" value="Macau">Macau</option>
<option class="option" value="Macedonia">Macedonia</option>
<option class="option" value="Madagascar">Madagascar</option>
<option class="option" value="Malawi">Malawi</option>
<option class="option" value="Malaysia">Malaysia</option>
<option class="option" value="Maloptiones">Maloptiones</option>
<option class="option" value="Mali">Mali</option>
<option class="option" value="Malta">Malta</option>
<option class="option" value="Martinique">Martinique</option>
<option class="option" value="Mauritania">Mauritania</option>
<option class="option" value="Mauritius">Mauritius</option>
<option class="option" value="Mayotte">Mayotte</option>
<option class="option" value="Mexico">Mexico</option>
<option class="option" value="Monaco">Monaco</option>
<option class="option" value="Mongolia">Mongolia</option>
<option class="option" value="Montserrat">Montserrat</option>
<option class="option" value="Morocco">Morocco</option>
<option class="option" value="Mozambique">Mozambique</option>
<option class="option" value="Myanmar">Myanmar</option>
<option class="option" value="Namibia">Namibia</option>
<option class="option" value="Nauru">Nauru</option>
<option class="option" value="Nepal">Nepal</option>
<option class="option" value="Netherlands">Netherlands</option>
<option class="option" value="New Caledonia">New Caledonia</option>
<option class="option" value="New Zealand">New Zealand</option>
<option class="option" value="Nicaragua">Nicaragua</option>
<option class="option" value="Niger">Niger</option>
<option class="option" value="Nigeria">Nigeria</option>
<option class="option" value="Niue">Niue</option>
<option class="option" value="Norfolk Island">Norfolk Island</option>
<option class="option" value="Norway">Norway</option>
<option class="option" value="Oman">Oman</option>
<option class="option" value="Pakistan">Pakistan</option>
<option class="option" value="Palau">Palau</option>
<option class="option" value="Palestine">Palestine</option>
<option class="option" value="Panama">Panama</option>
<option class="option" value="Papua New Guinea">Papua New Guinea</option>
<option class="option" value="Paraguay">Paraguay</option>
<option class="option" value="Peru">Peru</option>
<option class="option" value="Philippines">Philippines</option>
<option class="option" value="Pitcairn">Pitcairn</option>
<option class="option" value="Poland">Poland</option>
<option class="option" value="Portugal">Portugal</option>
<option class="option" value="Puerto Rico">Puerto Rico</option>
<option class="option" value="Qatar">Qatar</option>
<option class="option" value="Romania">Romania</option>
<option class="option" value="Russia">Russia</option>
<option class="option" value="Rwanda">Rwanda</option>
<option class="option" value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
<option class="option" value="Saint Lucia">Saint Lucia</option>
<option class="option" value="Samoa">Samoa</option>
<option class="option" value="San Marino">San Marino</option>
<option class="option" value="Sao Tome and Principe">Sao Tome and Principe</option>
<option class="option" value="Saudi Arabia">Saudi Arabia</option>
<option class="option" value="Serbia">Serbia</option>
<option class="option" value="Senegal">Senegal</option>
<option class="option" value="Seychelles">Seychelles</option>
<option class="option" value="Sierra Leone">Sierra Leone</option>
<option class="option" value="Singapore">Singapore</option>
<option class="option" value="Slovakia (Slovak Republic)">Slovakia (Slovak Republic)</option>
<option class="option" value="Slovenia">Slovenia</option>
<option class="option" value="Solomon Islands">Solomon Islands</option>
<option class="option" value="Somalia">Somalia</option>
<option class="option" value="South Africa">South Africa</option>
<option class="option" value="Spain">Spain</option>
<option class="option" value="Sri Lanka">Sri Lanka</option>
<option class="option" value="St. Helena">St. Helena</option>
<option class="option" value="Sudan">Sudan</option>
<option class="option" value="Suriname">Suriname</option>
<option class="option" value="Swaziland">Swaziland</option>
<option class="option" value="Sweden">Sweden</option>
<option class="option" value="Switzerland">Switzerland</option>
<option class="option" value="Syrian Arab Republic">Syrian Arab Republic</option>
<option class="option" value="Taiwan">Taiwan</option>
<option class="option" value="Tajikistan">Tajikistan</option>
<option class="option" value="Thailand">Thailand</option>
<option class="option" value="Togo">Togo</option>
<option class="option" value="Tokelau">Tokelau</option>
<option class="option" value="Tonga">Tonga</option>
<option class="option" value="Trinidad and Tobago">Trinidad and Tobago</option>
<option class="option" value="Tunisia">Tunisia</option>
<option class="option" value="Turkey">Turkey</option>
<option class="option" value="Turkmenistan">Turkmenistan</option>
<option class="option" value="Tuvalu">Tuvalu</option>
<option class="option" value="Uganda">Uganda</option>
<option class="option" value="Ukraine">Ukraine</option>
<option class="option" value="United Arab Emirates">United Arab Emirates</option>
<option class="option" value="United Kingdom">United Kingdom</option>
<option class="option" value="United States">United States</option>
<option class="option" value="Uruguay">Uruguay</option>
<option class="option" value="Uzbekistan">Uzbekistan</option>
<option class="option" value="Vanuatu">Vanuatu</option>
<option class="option" value="Venezuela">Venezuela</option>
<option class="option" value="Viet Nam">Viet Nam</option>
<option class="option" value="Virgin Islands (British)">Virgin Islands (British)</option>
<option class="option" value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
<option class="option" value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
<option class="option" value="Western Sahara">Western Sahara</option>
<option class="option" value="Yemen">Yemen</option>
<option class="option" value="Yugoslavia">Yugoslavia</option>
<option class="option" value="Zambia">Zambia</option>
<option class="option" value="Zimbabwe">Zimbabwe</option>
		</select>
    </div>
	
	<div class="editfield field_postal_code required-field visibility-public field_type_text" id="postal-code-field">
        <label for="postal_code"><?php _e('Postal Code', 'my-plugin'); ?> 
            <span class="bp-required-field-label"><?php _e('(required)', 'my-plugin'); ?></span>
        </label>
        <?php do_action( 'bp_country_errors' ); ?>
        <input type="text" id="postal_code" name="postal_code" value="<?php echo esc_attr($postal_code_field_value); ?>" />
    </div>
	
	<div class="editfield field_district required-field visibility-public field_type_text" id="district-field">
        <label for="district"><?php _e('District', 'my-plugin'); ?> 
            <span class="bp-required-field-label"><?php _e('(required)', 'my-plugin'); ?></span>
        </label>
        <?php do_action( 'bp_country_errors' ); ?>
        <input type="text" id="district" name="district" value="<?php echo esc_attr($district_field_value); ?>" />
    </div>
	
	<div class="editfield field_district required-field visibility-public field_type_text" id="preferred-languages-field">
        <label for="preferred_languages"><?php _e('Preferred Languages', 'my-plugin'); ?> 
            <span class="bp-required-field-label"><?php _e('(required)', 'my-plugin'); ?></span>
        </label>
        <?php do_action( 'bp_preferred_languages_errors' ); ?>
        <select type="text" id="preferred_languages" name="preferred_languages" >
			<option class="option" value="English">English</option>
			<option class="Germen" value="English">Germen</option>
		</select>
    </div>
	<h3>Where From you know from___community?</h3>
	<!-- Checkboxes -->
	<div class="editfield field_recommendation required-field visibility-public field_type_text" id="recommendation-field">
        <label for="recommendation"><?php _e('Recommendation(I know a xx-member)', 'my-plugin'); ?> 
            <span class="bp-required-field-label"><?php _e('(required)', 'my-plugin'); ?></span>
        </label>
        <?php do_action( 'bp_recommendation_errors' ); ?>
        <input type="checkbox" id="recommendation" name="recommendation" value="<?php echo esc_attr($recommendation_field_value); ?>" />
    </div>
	
	<div class="editfield field_commercial_media required-field visibility-public field_type_text" id="commercial_media-field">
        <label for="commercial_media"><?php _e('Commercial/ Internet Search/Social Media', 'my-plugin'); ?> 
            <span class="bp-required-field-label"><?php _e('(required)', 'my-plugin'); ?></span>
        </label>
        <?php do_action( 'bp_first_name_errors' ); ?>
        <input type="checkbox" id="commercial_media" name="commercial_media" value="<?php echo esc_attr($first_name_field_value); ?>" />
    </div>
	<!-- Checkboxes  End-->

	
	<div class="editfield field_message required-field visibility-public field_type_text" id="message-field">
        <label for="message"><?php _e('Message', 'my-plugin'); ?> 
            <span class="bp-required-field-label"><?php _e('(required)', 'my-plugin'); ?></span>
        </label>
 
        <?php do_action( 'bp_country_errors' ); ?>
        <input type="text" id="message" name="message" value="<?php echo esc_attr($message_field_value); ?>" />
    </div>
	
    <?php
});
if ( $_POST ) {
    add_action('bp_actions', function() {
        global $bp, $twitter_field_value;
 
        // making sure we are in the registration page
        if ( !function_exists('bp_is_current_component') || !bp_is_current_component('register') )
            return;
 
        // validating our custom field
       /*  if ( !isset($_POST['address']) || !trim($_POST['address']) ) {
            // we might be too early (priority 0), let's instantiate the errors array
            if ( !isset($bp->signup->errors) ) {
                $bp->signup->errors = array();
            }
 
            // now let's enter a custom error message
            // please make sure the errors index key is your custom field's name
            $bp->signup->errors['address'] = __('Please enter your Address', 'my-plugin');
        } else {
            $twitter_field_value = sanitize_text_field($_POST['address']);
        } */
    }, 0);
}

add_filter('bp_signup_usermeta', function($usermeta){
    global $twitter_field_value;
 
    return array_merge(array('address' => $twitter_field_value), $usermeta);
});
add_filter('bp_core_activated_user', function($user_id, $key, $user){
    $tag = 'address';
 
    if ( $user_id && !empty( $user['meta'][$tag] ) ) {
        return update_user_meta( $user_id, "bp_custom_{$tag}", $user['meta'][$tag] );
    }
}, 10, 3 );
add_action('bp_profile_header_meta', function(){
    $user_id = bp_displayed_user_id();
 
    $address_value = get_user_meta( $user_id, 'bp_custom_address', 1 );
 
    if ( $address_value ) {
        print '<p class="bbp-user-address-data">' . (
            sprintf( __( '@%s on address', 'my-plugin' ), $address_value )
        ) . '</p>';
    }
});
add_action('bp_core_general_settings_before_submit', function() {
    $user_id = bp_displayed_user_id();
 
    ?>
	
    <div id="title-handle">
        <label for="title"><?php _e('Title', 'my-plugin'); ?></label>
        <?php $title_field_value1 = esc_attr( get_user_meta( $user_id, 'bp_title', 1 ) ); ?>
		<select type="text" id="title" name="title" >
		<?php if ($title_field_value1) echo '<option value="'. $title_field_value1.'" selected>'. $title_field_value1.'</option>';?>
		<option class="option" value="">--- Select ---</option>
		<option class="option" value="mr">Mr.</option>
		<option class="option" value="miss">Miss.</option>
		<option class="option" value="mrs">Mrs.</option>
		</select>
    </div>	
	

    <div id="first_name-handle">
        <label for="first_name"><?php _e('First Name', 'my-plugin'); ?></label>
        <input type="text" id="first_name" name="first_name" value="<?php echo esc_attr( get_user_meta( $user_id, 'bp_first_name', 1 ) ); ?>" />
    </div>
	
    <div id="last_name-handle">
        <label for="last_name"><?php _e('Last Name', 'my-plugin'); ?></label>
        <input type="text" id="last_name" name="last_name" value="<?php echo esc_attr( get_user_meta( $user_id, 'bp_last_name', 1 ) ); ?>" />
    </div>
	
	<div id="address-handle">
        <label for="address"><?php _e('Address', 'my-plugin'); ?></label>
        <input type="text" id="address" name="address" value="<?php echo esc_attr( get_user_meta( $user_id, 'bp_custom_address', 1 ) ); ?>" />
    </div>
	<div id="telephone-handle">
        <label for="address"><?php _e('Telephone', 'my-plugin'); ?></label>
        <input type="text" id="telephone" name="telephone" value="<?php echo esc_attr( get_user_meta( $user_id, 'bp_telephone', 1 ) ); ?>" />
    </div>
	 <div id="city-handle">
        <label for="address"><?php _e('City', 'my-plugin'); ?></label>
        <input type="text" id="city" name="city" value="<?php echo esc_attr( get_user_meta( $user_id, 'bp_city', 1 ) ); ?>" />
    </div>
	 <div id="country-handle">
        <label for="country"><?php _e('Country', 'my-plugin'); ?></label>
        <?php $country_field_value1 = esc_attr( get_user_meta( $user_id, 'bp_country', 1 ) ); ?>
		<select type="text" id="country" name="country">
		<?php if ($country_field_value1) echo '<option value="'. $country_field_value1.'" selected>'. $country_field_value1.'</option>';?>	
<option class="option" value="">--- Select ---</option>
<option class="option" value="United Arab Emirates">United Arab Emirates</option>
<option class="option" value="Albania">Albania</option>
<option class="option" value="Algeria">Algeria</option>
<option class="option" value="American Samoa">American Samoa</option>
<option class="option" value="Andorra">Andorra</option>
<option class="option" value="Angola">Angola</option>
<option class="option" value="Anguilla">Anguilla</option>
<option class="option" value="Antarctica">Antarctica</option>
<option class="option" value="Antigua and Barbuda">Antigua and Barbuda</option>
<option class="option" value="Argentina">Argentina</option>
<option class="option" value="Armenia">Armenia</option>
<option class="option" value="Aruba">Aruba</option>
<option class="option" value="Australia">Australia</option>
<option class="option" value="Austria">Austria</option>
<option class="option" value="Azerbaijan">Azerbaijan</option>
<option class="option" value="Bahamas">Bahamas</option>
<option class="option" value="Bahrain">Bahrain</option>
<option class="option" value="Bangladesh">Bangladesh</option>
<option class="option" value="Barbados">Barbados</option>
<option class="option" value="Belarus">Belarus</option>
<option class="option" value="Belgium">Belgium</option>
<option class="option" value="Belize">Belize</option>
<option class="option" value="Benin">Benin</option>
<option class="option" value="Bermuda">Bermuda</option>
<option class="option" value="Bhutan">Bhutan</option>
<option class="option" value="Bolivia">Bolivia</option>
<option class="option" value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
<option class="option" value="Botswana">Botswana</option>
<option class="option" value="Bouvet Island">Bouvet Island</option>
<option class="option" value="Brazil">Brazil</option>
<option class="option" value="Brunei Darussalam">Brunei Darussalam</option>
<option class="option" value="Bulgaria">Bulgaria</option>
<option class="option" value="Burkina Faso">Burkina Faso</option>
<option class="option" value="Burundi">Burundi</option>
<option class="option" value="Cambodia">Cambodia</option>
<option class="option" value="Cameroon">Cameroon</option>
<option class="option" value="Canada">Canada</option>
<option class="option" value="Cape Verde">Cape Verde</option>
<option class="option" value="Cayman Islands">Cayman Islands</option>
<option class="option" value="Central African Republic">Central African Republic</option>
<option class="option" value="Chad">Chad</option>
<option class="option" value="Chile">Chile</option>
<option class="option" value="China">China</option>
<option class="option" value="Christmas Island">Christmas Island</option>
<option class="option" value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
<option class="option" value="Colombia">Colombia</option>
<option class="option" value="Comoros">Comoros</option>
<option class="option" value="Congo">Congo</option>
<option class="option" value="Cook Islands">Cook Islands</option>
<option class="option" value="Costa Rica">Costa Rica</option>
<option class="option" value="Cote optionoire">Cote optionoire</option>
<option class="option" value="Croatia">Croatia</option>
<option class="option" value="Cuba">Cuba</option>
<option class="option" value="Cyprus">Cyprus</option>
<option class="option" value="Czech Republic">Czech Republic</option>
<option class="option" value="Denmark">Denmark</option>
<option class="option" value="Djibouti">Djibouti</option>
<option class="option" value="Dominica">Dominica</option>
<option class="option" value="Dominican Republic">Dominican Republic</option>
<option class="option" value="East Timor">East Timor</option>
<option class="option" value="Ecuador">Ecuador</option>
<option class="option" value="Egypt">Egypt</option>
<option class="option" value="El Salvador">El Salvador</option>
<option class="option" value="Equatorial Guinea">Equatorial Guinea</option>
<option class="option" value="Eritrea">Eritrea</option>
<option class="option" value="Estonia">Estonia</option>
<option class="option" value="Ethiopia">Ethiopia</option>
<option class="option" value="Faroe Islands">Faroe Islands</option>
<option class="option" value="Fiji">Fiji</option>
<option class="option" value="Finland">Finland</option>
<option class="option" value="France">France</option>
<option class="option" value="France Metropolitan">France Metropolitan</option>
<option class="option" value="French Guiana">French Guiana</option>
<option class="option" value="French Polynesia">French Polynesia</option>
<option class="option" value="Gabon">Gabon</option>
<option class="option" value="Gambia">Gambia</option>
<option class="option" value="Georgia">Georgia</option>
<option class="option" value="Germany">Germany</option>
<option class="option" value="Ghana">Ghana</option>
<option class="option" value="Gibraltar">Gibraltar</option>
<option class="option" value="Greece">Greece</option>
<option class="option" value="Greenland">Greenland</option>
<option class="option" value="Grenada">Grenada</option>
<option class="option" value="Guadeloupe">Guadeloupe</option>
<option class="option" value="Guam">Guam</option>
<option class="option" value="Guatemala">Guatemala</option>
<option class="option" value="Guinea">Guinea</option>
<option class="option" value="Guinea-bissau">Guinea-bissau</option>
<option class="option" value="Guyana">Guyana</option>
<option class="option" value="Haiti">Haiti</option>
<option class="option" value="Honduras">Honduras</option>
<option class="option" value="Hong Kong">Hong Kong</option>
<option class="option" value="Hungary">Hungary</option>
<option class="option" value="Iceland">Iceland</option>
<option class="option" value="India">India</option>
<option class="option" value="Indonesia">Indonesia</option>
<option class="option" value="Iran">Iran</option>
<option class="option" value="Iraq">Iraq</option>
<option class="option" value="Ireland">Ireland</option>
<option class="option" value="Italy">Italy</option>
<option class="option" value="Jamaica">Jamaica</option>
<option class="option" value="Japan">Japan</option>
<option class="option" value="Jordan">Jordan</option>
<option class="option" value="Kazakhstan">Kazakhstan</option>
<option class="option" value="Kenya">Kenya</option>
<option class="option" value="Kiribati">Kiribati</option>
<option class="option" value="Korea">Korea</option>
<option class="option" value="Kuwait">Kuwait</option>
<option class="option" value="Kyrgyzstan">Kyrgyzstan</option>
<option class="option" value="Latvia">Latvia</option>
<option class="option" value="Lebanon">Lebanon</option>
<option class="option" value="Lesotho">Lesotho</option>
<option class="option" value="Liberia">Liberia</option>
<option class="option" value="Libya">Libya</option>
<option class="option" value="Liechtenstein">Liechtenstein</option>
<option class="option" value="Lithuania">Lithuania</option>
<option class="option" value="Luxembourg">Luxembourg</option>
<option class="option" value="Macau">Macau</option>
<option class="option" value="Macedonia">Macedonia</option>
<option class="option" value="Madagascar">Madagascar</option>
<option class="option" value="Malawi">Malawi</option>
<option class="option" value="Malaysia">Malaysia</option>
<option class="option" value="Maloptiones">Maloptiones</option>
<option class="option" value="Mali">Mali</option>
<option class="option" value="Malta">Malta</option>
<option class="option" value="Martinique">Martinique</option>
<option class="option" value="Mauritania">Mauritania</option>
<option class="option" value="Mauritius">Mauritius</option>
<option class="option" value="Mayotte">Mayotte</option>
<option class="option" value="Mexico">Mexico</option>
<option class="option" value="Monaco">Monaco</option>
<option class="option" value="Mongolia">Mongolia</option>
<option class="option" value="Montserrat">Montserrat</option>
<option class="option" value="Morocco">Morocco</option>
<option class="option" value="Mozambique">Mozambique</option>
<option class="option" value="Myanmar">Myanmar</option>
<option class="option" value="Namibia">Namibia</option>
<option class="option" value="Nauru">Nauru</option>
<option class="option" value="Nepal">Nepal</option>
<option class="option" value="Netherlands">Netherlands</option>
<option class="option" value="New Caledonia">New Caledonia</option>
<option class="option" value="New Zealand">New Zealand</option>
<option class="option" value="Nicaragua">Nicaragua</option>
<option class="option" value="Niger">Niger</option>
<option class="option" value="Nigeria">Nigeria</option>
<option class="option" value="Niue">Niue</option>
<option class="option" value="Norfolk Island">Norfolk Island</option>
<option class="option" value="Norway">Norway</option>
<option class="option" value="Oman">Oman</option>
<option class="option" value="Pakistan">Pakistan</option>
<option class="option" value="Palau">Palau</option>
<option class="option" value="Palestine">Palestine</option>
<option class="option" value="Panama">Panama</option>
<option class="option" value="Papua New Guinea">Papua New Guinea</option>
<option class="option" value="Paraguay">Paraguay</option>
<option class="option" value="Peru">Peru</option>
<option class="option" value="Philippines">Philippines</option>
<option class="option" value="Pitcairn">Pitcairn</option>
<option class="option" value="Poland">Poland</option>
<option class="option" value="Portugal">Portugal</option>
<option class="option" value="Puerto Rico">Puerto Rico</option>
<option class="option" value="Qatar">Qatar</option>
<option class="option" value="Romania">Romania</option>
<option class="option" value="Russia">Russia</option>
<option class="option" value="Rwanda">Rwanda</option>
<option class="option" value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
<option class="option" value="Saint Lucia">Saint Lucia</option>
<option class="option" value="Samoa">Samoa</option>
<option class="option" value="San Marino">San Marino</option>
<option class="option" value="Sao Tome and Principe">Sao Tome and Principe</option>
<option class="option" value="Saudi Arabia">Saudi Arabia</option>
<option class="option" value="Serbia">Serbia</option>
<option class="option" value="Senegal">Senegal</option>
<option class="option" value="Seychelles">Seychelles</option>
<option class="option" value="Sierra Leone">Sierra Leone</option>
<option class="option" value="Singapore">Singapore</option>
<option class="option" value="Slovakia (Slovak Republic)">Slovakia (Slovak Republic)</option>
<option class="option" value="Slovenia">Slovenia</option>
<option class="option" value="Solomon Islands">Solomon Islands</option>
<option class="option" value="Somalia">Somalia</option>
<option class="option" value="South Africa">South Africa</option>
<option class="option" value="Spain">Spain</option>
<option class="option" value="Sri Lanka">Sri Lanka</option>
<option class="option" value="St. Helena">St. Helena</option>
<option class="option" value="Sudan">Sudan</option>
<option class="option" value="Suriname">Suriname</option>
<option class="option" value="Swaziland">Swaziland</option>
<option class="option" value="Sweden">Sweden</option>
<option class="option" value="Switzerland">Switzerland</option>
<option class="option" value="Syrian Arab Republic">Syrian Arab Republic</option>
<option class="option" value="Taiwan">Taiwan</option>
<option class="option" value="Tajikistan">Tajikistan</option>
<option class="option" value="Thailand">Thailand</option>
<option class="option" value="Togo">Togo</option>
<option class="option" value="Tokelau">Tokelau</option>
<option class="option" value="Tonga">Tonga</option>
<option class="option" value="Trinidad and Tobago">Trinidad and Tobago</option>
<option class="option" value="Tunisia">Tunisia</option>
<option class="option" value="Turkey">Turkey</option>
<option class="option" value="Turkmenistan">Turkmenistan</option>
<option class="option" value="Tuvalu">Tuvalu</option>
<option class="option" value="Uganda">Uganda</option>
<option class="option" value="Ukraine">Ukraine</option>
<option class="option" value="United Arab Emirates">United Arab Emirates</option>
<option class="option" value="United Kingdom">United Kingdom</option>
<option class="option" value="United States">United States</option>
<option class="option" value="Uruguay">Uruguay</option>
<option class="option" value="Uzbekistan">Uzbekistan</option>
<option class="option" value="Vanuatu">Vanuatu</option>
<option class="option" value="Venezuela">Venezuela</option>
<option class="option" value="Viet Nam">Viet Nam</option>
<option class="option" value="Virgin Islands (British)">Virgin Islands (British)</option>
<option class="option" value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
<option class="option" value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
<option class="option" value="Western Sahara">Western Sahara</option>
<option class="option" value="Yemen">Yemen</option>
<option class="option" value="Yugoslavia">Yugoslavia</option>
<option class="option" value="Zambia">Zambia</option>
<option class="option" value="Zimbabwe">Zimbabwe</option>
		</select>
    </div>
	 <div id="postal_code-handle">
        <label for="postal_code"><?php _e('Postal Code', 'my-plugin'); ?></label>
        <input type="text" id="postal_code" name="postal_code" value="<?php echo esc_attr( get_user_meta( $user_id, 'bp_postal_code', 1 ) ); ?>" />
    </div> 
	<div id="district-handle">
        <label for="district"><?php _e('District', 'my-plugin'); ?></label>
        <input type="text" id="district" name="district" value="<?php echo esc_attr( get_user_meta( $user_id, 'bp_district', 1 ) ); ?>" />
    </div>
	<div id="preferred_languages-handle">
        <label for="preferred_languages"><?php _e('Preferred Languages', 'my-plugin'); ?></label>
        <?php $preferred_languages_field_value = esc_attr( get_user_meta( $user_id, 'bp_preferred_languages', 1 ) ); ?>
		
		<select type="text" id="preferred_languages" name="preferred_languages" >
			<?php if ($preferred_languages_field_value) echo '<option value="'. $preferred_languages_field_value.'" selected>'. $preferred_languages_field_value.'</option>';?>
			<option class="option" value="english">English</option>
			<option class="option" value="germen">Germen</option>	
		</select>
    </div>

	<!-- Custom Fields -->
	    <h3>Where From you know from___community?</h3>
		<?php $recommendation_val = esc_attr( get_user_meta( $user_id, 'bp_recommendation', 1 ) ); ?>
		<div id="recommendation-handle">
			<label for="recommendation"><?php _e('Recommendation', 'my-plugin'); ?></label>
			<input type="checkbox" id="recommendation" name="recommendation" <?php if ($recommendation_val) echo 'checked'; ?> >
		</div>
		
		<?php $commercial_media_val = esc_attr( get_user_meta( $user_id, 'bp_commercial_media', 1 ) ); ?>
		<div id="commercial_media-handle">
			<label for="commercial_media"><?php _e('Commercial Media', 'my-plugin'); ?></label>
			<input type="checkbox" id="commercial_media" name="commercial_media"  <?php if ($commercial_media_val) echo 'checked'; ?>>
		</div>	
	<!-- End Custom Fields -->

	<div id="Message-handle">
        <label for="address"><?php _e('Message', 'my-plugin'); ?></label>
        <input type="text" id="message" name="message" value="<?php echo esc_attr( get_user_meta( $user_id, 'bp_message', 1 ) ); ?>" />
    </div>
    <?php
 
    // some nonce
    wp_nonce_field( 'twitter_nonce', 'twitter_nonce' );
});
add_action('bp_core_general_settings_after_save', function() {
    if ( !isset($_POST['twitter_nonce'], $_POST['address']) || !wp_verify_nonce( $_POST['twitter_nonce'], 'twitter_nonce' ) )
        return;
 
    $user_id = bp_displayed_user_id();
    list($new_address, $old_address) = array( sanitize_text_field($_POST['address']), get_user_meta( $user_id, 'bp_custom_address', 1 ) );  
 
    if ( $new_address != $old_address ) {
        if ( $new_address ) {
            update_user_meta( $user_id, 'bp_custom_address', $new_address );
        } else {
            // perhaps you want to keep things required, so you might want to throw an error here
            // but with me, it's fine.
            delete_user_meta( $user_id, 'bp_custom_address' );
        }
        bp_core_add_message( __('Your address was successfully updated.', 'my-plugin') . PHP_EOL, 'success');
    }
	if ( !isset($_POST['twitter_nonce'], $_POST['city']) || !wp_verify_nonce( $_POST['twitter_nonce'], 'twitter_nonce' ) )
        return;
 
    list($new_city, $old_city) = array( sanitize_text_field($_POST['city']), get_user_meta( $user_id, 'bp_custom_city', 1 ) );  
 
    if ( $new_city != $old_city ) {
        if ( $new_city ) {
            update_user_meta( $user_id, 'bp_city', $new_city );
        } else {
            // perhaps you want to keep things required, so you might want to throw an error here
            // but with me, it's fine.
            delete_user_meta( $user_id, 'bp_custom_city' );
        }
        bp_core_add_message( __('Your city was successfully updated.', 'my-plugin') . PHP_EOL, 'success');
    }

	//Recommendation Block to copy
	if ( !isset($_POST['twitter_nonce'], $_POST['recommendation']) || !wp_verify_nonce( $_POST['twitter_nonce'], 'twitter_nonce' ) )
        return;
 
    list($new_recommendation, $old_telephone) = array( sanitize_text_field($_POST['recommendation']), get_user_meta( $user_id, 'bp_telephone', 1 ) );  
 
    if ( $new_recommendation != $old_telephone ) {
        if ( $new_recommendation ) {
            update_user_meta( $user_id, 'bp_recommendation', $new_recommendation );
        } else {
            // perhaps you want to keep things required, so you might want to throw an error here
            // but with me, it's fine.
            delete_user_meta( $user_id, 'bp_recommendation' );
        }
        bp_core_add_message( __('Your Recommendation was successfully updated.', 'my-plugin') . PHP_EOL, 'success');
    }
	//Block Ends	

	//Communication Media Block to copy
	if ( !isset($_POST['twitter_nonce'], $_POST['recommendation']) || !wp_verify_nonce( $_POST['twitter_nonce'], 'twitter_nonce' ) )
        return;
 
    list($commercial_media, $old_telephone) = array( sanitize_text_field($_POST['commercial_media']), get_user_meta( $user_id, 'bp_telephone', 1 ) );  
 
    if ( $commercial_media != $old_telephone ) {
        if ( $commercial_media ) {
            update_user_meta( $user_id, 'bp_commercial_media', $commercial_media );
        } else {
            // perhaps you want to keep things required, so you might want to throw an error here
            // but with me, it's fine.
            delete_user_meta( $user_id, 'bp_commercial_media' );
        }
        bp_core_add_message( __('Your Commercial Media was successfully updated.', 'my-plugin') . PHP_EOL, 'success');
    }
	//Block Ends	

	//Block to copy
	if ( !isset($_POST['twitter_nonce'], $_POST['telephone']) || !wp_verify_nonce( $_POST['twitter_nonce'], 'twitter_nonce' ) )
        return;
 
    list($new_telephone, $old_telephone) = array( sanitize_text_field($_POST['telephone']), get_user_meta( $user_id, 'bp_telephone', 1 ) );  
 
    if ( $new_telephone != $old_telephone ) {
        if ( $new_telephone ) {
            update_user_meta( $user_id, 'bp_telephone', $new_telephone );
        } else {
            // perhaps you want to keep things required, so you might want to throw an error here
            // but with me, it's fine.
            delete_user_meta( $user_id, 'bp_telephone' );
        }
        bp_core_add_message( __('Your telephone was successfully updated.', 'my-plugin') . PHP_EOL, 'success');
    }
	//Block Ends	

	//Block Title
	if ( !isset($_POST['twitter_nonce'], $_POST['title']) || !wp_verify_nonce( $_POST['twitter_nonce'], 'twitter_nonce' ) )
        return;
 
    list($new_title, $old_telephone) = array( sanitize_text_field($_POST['title']), get_user_meta( $user_id, 'bp_title', 1 ) );  
 
    if ( $new_title != $old_telephone ) {
        if ( $new_title ) {
            update_user_meta( $user_id, 'bp_title', $new_title );
        } else {
            // perhaps you want to keep things required, so you might want to throw an error here
            // but with me, it's fine.
            delete_user_meta( $user_id, 'bp_title' );
        }
        bp_core_add_message( __('Your Title was successfully updated.', 'my-plugin') . PHP_EOL, 'success');
    }
	//Block Ends Title	

	//Block First name
	if ( !isset($_POST['twitter_nonce'], $_POST['first_name']) || !wp_verify_nonce( $_POST['twitter_nonce'], 'twitter_nonce' ) )
        return;
 
    list($new_first_name, $old_telephone) = array( sanitize_text_field($_POST['first_name']), get_user_meta( $user_id, 'bp_first_name', 1 ) );  
 
    if ( $new_first_name != $old_telephone ) {
        if ( $new_first_name ) {
            update_user_meta( $user_id, 'bp_first_name', $new_first_name );
        } else {
            // perhaps you want to keep things required, so you might want to throw an error here
            // but with me, it's fine.
            delete_user_meta( $user_id, 'bp_first_name' );
        }
        bp_core_add_message( __('Your First Name was successfully updated.', 'my-plugin') . PHP_EOL, 'success');
    }
	//Block Ends First Name	

	//Block Last name
	if ( !isset($_POST['twitter_nonce'], $_POST['last_name']) || !wp_verify_nonce( $_POST['twitter_nonce'], 'twitter_nonce' ) )
        return;
 
    list($new_last_name, $old_telephone) = array( sanitize_text_field($_POST['last_name']), get_user_meta( $user_id, 'bp_last_name', 1 ) );  
 
    if ( $new_last_name != $old_telephone ) {
        if ( $new_last_name ) {
            update_user_meta( $user_id, 'bp_last_name', $new_last_name );
        } else {
            // perhaps you want to keep things required, so you might want to throw an error here
            // but with me, it's fine.
            delete_user_meta( $user_id, 'bp_last_name' );
        }
        bp_core_add_message( __('Your Last Name was successfully updated.', 'my-plugin') . PHP_EOL, 'success');
    }
	//Block Ends Last Name		

	//Block Pref Language
	if ( !isset($_POST['twitter_nonce'], $_POST['preferred_languages']) || !wp_verify_nonce( $_POST['twitter_nonce'], 'twitter_nonce' ) )
        return;
 
    list($new_preferred_languages, $old_telephone) = array( sanitize_text_field($_POST['preferred_languages']), get_user_meta( $user_id, 'bp_preferred_languages', 1 ) );  
 
    if ( $new_preferred_languages != $old_telephone ) {
        if ( $new_preferred_languages ) {
            update_user_meta( $user_id, 'bp_preferred_languages', $new_preferred_languages );
        } else {
            // perhaps you want to keep things required, so you might want to throw an error here
            // but with me, it's fine.
            delete_user_meta( $user_id, 'bp_preferred_languages' );
        }
        bp_core_add_message( __('Your Preferred Languages was successfully updated.', 'my-plugin') . PHP_EOL, 'success');
    }
	//Block Pref Language


	if ( !isset($_POST['twitter_nonce'], $_POST['postal_code']) || !wp_verify_nonce( $_POST['twitter_nonce'], 'twitter_nonce' ) )
        return;
 
    list($new_postal_code, $old_postal_code) = array( sanitize_text_field($_POST['postal_code']), get_user_meta( $user_id, 'bp_postal_code', 1 ) );  
 
    if ( $new_postal_code != $old_postal_code ) {
        if ( $new_postal_code ) {
            update_user_meta( $user_id, 'bp_postal_code', $new_postal_code );
        } else {
            // perhaps you want to keep things required, so you might want to throw an error here
            // but with me, it's fine.
            delete_user_meta( $user_id, 'bp_postal_code' );
        }
        bp_core_add_message( __('Your postal_code was successfully updated.', 'my-plugin') . PHP_EOL, 'success');
    }
	if ( !isset($_POST['twitter_nonce'], $_POST['district']) || !wp_verify_nonce( $_POST['twitter_nonce'], 'twitter_nonce' ) )
        return;
     list($new_district, $old_district) = array( sanitize_text_field($_POST['district']), get_user_meta( $user_id, 'bp_district', 1 ) );  
 
    if ( $new_district != $old_district ) {
        if ( $new_district ) {
            update_user_meta( $user_id, 'bp_district', $new_district );
        } else {
            // perhaps you want to keep things required, so you might want to throw an error here
            // but with me, it's fine.
            delete_user_meta( $user_id, 'bp_district' );
        }
        bp_core_add_message( __('Your district was successfully updated.', 'my-plugin') . PHP_EOL, 'success');
    }
	if ( !isset($_POST['twitter_nonce'], $_POST['message']) || !wp_verify_nonce( $_POST['twitter_nonce'], 'twitter_nonce' ) )
        return;
     list($new_message, $old_message) = array( sanitize_text_field($_POST['message']), get_user_meta( $user_id, 'bp_message', 1 ) );  
 
    if ( $new_message != $old_message ) {
        if ( $new_message ) {
            update_user_meta( $user_id, 'bp_message', $new_message );
        } else {
            // perhaps you want to keep things required, so you might want to throw an error here
            // but with me, it's fine.
            delete_user_meta( $user_id, 'bp_message' );
        }
        bp_core_add_message( __('Your message was successfully updated.', 'my-plugin') . PHP_EOL, 'success');
    }
if ( !isset($_POST['twitter_nonce'], $_POST['country']) || !wp_verify_nonce( $_POST['twitter_nonce'], 'twitter_nonce' ) )
        return;
     list($new_country, $old_country) = array( sanitize_text_field($_POST['country']), get_user_meta( $user_id, 'bp_country', 1 ) );  
 
    if ( $new_country != $old_country ) {
        if ( $new_country ) {
            update_user_meta( $user_id, 'bp_country', $new_country );
        } else {
            // perhaps you want to keep things required, so you might want to throw an error here
            // but with me, it's fine.
            delete_user_meta( $user_id, 'bp_country' );
        }
        bp_core_add_country( __('Your country was successfully updated.', 'my-plugin') . PHP_EOL, 'success');
    }
});








?>